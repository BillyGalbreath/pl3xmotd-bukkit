package net.pl3x.bukkit.pl3xmotd.configuration;

import net.pl3x.bukkit.pl3xmotd.Logger;
import net.pl3x.bukkit.pl3xmotd.Pl3xMOTD;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class MOTDConfig extends YamlConfiguration {
    private static MOTDConfig config;

    public static MOTDConfig getConfig() {
        if (config == null) {
            config = new MOTDConfig();
        }
        return config;
    }

    private final File file;

    private MOTDConfig() {
        super();
        file = new File(Pl3xMOTD.getPlugin(Pl3xMOTD.class).getDataFolder(), "motds.yml");
        loadConfiguration(file);
    }

    private void load() {
        try {
            load(file);
        } catch (IOException | InvalidConfigurationException e) {
            Logger.error("[ERROR] Error while loading motds.yml");
        }
    }

    public void reload() {
        loadConfiguration(file);
        load();
    }

    public void saveDefaultConfig() {
        if (!file.exists()) {
            Pl3xMOTD.getPlugin(Pl3xMOTD.class).saveResource("motds.yml", false);
        }

        if (file.exists()) {
            loadConfiguration(file);
            load();
        }
    }

    public List<String> getMOTDs() {
        return getStringList("motds");
    }
}
