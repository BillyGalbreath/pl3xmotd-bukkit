package net.pl3x.bukkit.pl3xmotd;

import net.pl3x.bukkit.pl3xmotd.commands.CmdPl3xMOTD;
import net.pl3x.bukkit.pl3xmotd.configuration.Config;
import net.pl3x.bukkit.pl3xmotd.configuration.Lang;
import net.pl3x.bukkit.pl3xmotd.configuration.MOTDConfig;
import net.pl3x.bukkit.pl3xmotd.listeners.MOTDListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xMOTD extends JavaPlugin {
    private long startTime = 0;

    public void onLoad() {
        startTime = System.currentTimeMillis();
    }

    public void onEnable() {
        Config.reload();
        Lang.reload();

        MOTDConfig.getConfig().saveDefaultConfig();

        Bukkit.getPluginManager().registerEvents(new MOTDListener(this), this);

        getCommand("pl3xmotd").setExecutor(new CmdPl3xMOTD(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    public void onDisable() {
        Logger.info(getName() + " Disabled.");
    }

    public static Pl3xMOTD getPlugin() {
        return Pl3xMOTD.getPlugin(Pl3xMOTD.class);
    }

    public long getStartTime() {
        return startTime;
    }
}
