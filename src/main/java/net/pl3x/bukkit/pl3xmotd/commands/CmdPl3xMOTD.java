package net.pl3x.bukkit.pl3xmotd.commands;

import net.pl3x.bukkit.pl3xmotd.Pl3xMOTD;
import net.pl3x.bukkit.pl3xmotd.configuration.Config;
import net.pl3x.bukkit.pl3xmotd.configuration.Lang;
import net.pl3x.bukkit.pl3xmotd.configuration.MOTDConfig;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.Collections;
import java.util.List;

public class CmdPl3xMOTD implements TabExecutor {
    private final Pl3xMOTD plugin;

    public CmdPl3xMOTD(Pl3xMOTD plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 1 && "reload".startsWith(args[0].toLowerCase())) {
            return Collections.singletonList("reload");
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if (!sender.hasPermission("command.pl3xmotd")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length > 0 && args[0].equalsIgnoreCase("reload")) {
            Config.reload();
            Lang.reload();

            MOTDConfig.getConfig().reload();

            Lang.send(sender, Lang.RELOAD
                    .replace("{plugin}", plugin.getName())
                    .replace("{version}", plugin.getDescription().getVersion()));
            return true;
        }

        Lang.send(sender, Lang.VERSION
                .replace("{plugin}", plugin.getName())
                .replace("{version}", plugin.getDescription().getVersion()));
        return true;
    }
}
