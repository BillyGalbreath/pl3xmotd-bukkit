package net.pl3x.bukkit.pl3xmotd.listeners;

import net.pl3x.bukkit.pl3xmotd.Logger;
import net.pl3x.bukkit.pl3xmotd.Pl3xMOTD;
import net.pl3x.bukkit.pl3xmotd.configuration.MOTDConfig;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.util.CachedServerIcon;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MOTDListener implements Listener {
    private Pl3xMOTD plugin;
    private final Random random;

    public MOTDListener(Pl3xMOTD plugin) {
        this.plugin = plugin;
        random = new Random();
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void MotdOnPing(ServerListPingEvent event) {
        String motd = getRandomMessage();
        if (motd == null || motd.isEmpty()) {
            Logger.debug("No MOTD was loaded/found. Falling back to server.properties value");
            return;
        }

        String message = setVariables(motd, event.getAddress().getHostAddress());
        if (message == null || message.equals("")) {
            Logger.warn("No MOTD was loaded/found. Falling back to server.properties value");
            return;
        }
        event.setMotd(ChatColor.translateAlternateColorCodes('&', message));
        Logger.debug("Custom MOTD set.");
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void IconOnPing(ServerListPingEvent event) {
        File dir = new File(Pl3xMOTD.getPlugin(Pl3xMOTD.class).getDataFolder() + File.separator + "server-icon");
        if (!dir.exists()) {
            //noinspection ResultOfMethodCallIgnored
            dir.mkdirs(); // create missing directory
            Logger.debug("No custom server-icon directory found, falling back to server default behavior.");
            return;
        }
        File[] iconFiles = dir.listFiles((dir1, name) -> name.endsWith(".png"));
        if (iconFiles == null || iconFiles.length < 1) {
            Logger.debug("No custom server-icon images found, falling back to server default behavior.");
            return;
        }
        int iconchoice = new Random().nextInt(iconFiles.length);
        CachedServerIcon icon = null;
        try {
            icon = Bukkit.loadServerIcon(iconFiles[iconchoice]);
        } catch (Exception e) {
            Logger.warn("Problem loading custom server-icon: &7" + e.getLocalizedMessage());
        }
        if (icon == null) {
            Logger.debug("Could not load custom server-icon, falling back to server default behavior.");
            return;
        }
        event.setServerIcon(icon);
        Logger.debug("Custom server-icon set.");
    }

    private String getRandomMessage() {
        List<String> motd = MOTDConfig.getConfig().getMOTDs();
        if (motd.size() == 0) {
            return null;
        }
        return motd.get(new Random().nextInt(motd.size()));
    }

    private String setVariables(String message, String ip) {
        String version = Bukkit.getServer().getBukkitVersion().replace("-SNAPSHOT", "");
        String packageName = Bukkit.getServer().getClass().getPackage().getName();
        String nmsVersion = packageName.substring(packageName.lastIndexOf('.') + 1).replace("v", "");
        String tpsfull = "unkown";
        int tps = 0;
        try {
            Object server = Class.forName("net.minecraft.server.v" + nmsVersion + ".MinecraftServer").getMethod("getServer").invoke(null);
            Field fTps = server.getClass().getField("recentTps");
            double[] realtps = (double[]) fTps.get(server);
            tpsfull = ((int) realtps[0]) + "," + ((int) realtps[1]) + "," + ((int) realtps[2]);
            tps = (int) realtps[0];
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }

        int seconds = (int) (System.currentTimeMillis() - plugin.getStartTime()) / 1000;
        String uptime;
        long minutes = seconds % 3600 / 60;
        long hours = seconds % 86400 / 3600;
        long days = seconds / 86400;
        if (seconds >= 86400) {
            uptime = String.format("%d:%d:%02d", days, hours, minutes);
        } else {
            uptime = String.format("%d:%02d", hours, minutes);
        }

        message = message
                .replace("{playerip}", ip)
                .replace("{onlineplayers}", Integer.toString(Bukkit.getOnlinePlayers().size()))
                .replace("{maxplayers}", Integer.toString(Bukkit.getMaxPlayers()))
                .replace("{uptime}", uptime)
                .replace("{tps}", Double.toString(tps))
                .replace("{tpsfull}", tpsfull)
                .replace("{serverid}", Bukkit.getServerId())
                .replace("{version}", version)
                .replace("{nmsversion}", nmsVersion)
                .replace("{shortversion}", version.split("-")[0])
                .replace("\\n", "\n");
        while (message.contains("&x")) {
            message = message.replaceFirst("&x", getRandomColor());
        }
        return message;
    }

    private String getRandomColor() {
        List<String> colors = Arrays.asList("&0", "&1", "&2", "&3", "&4", "&5", "&6", "&7", "&8", "&9", "&a", "&b", "&c", "&d", "&e", "&f");
        return colors.get(random.nextInt(colors.size()));
    }
}
